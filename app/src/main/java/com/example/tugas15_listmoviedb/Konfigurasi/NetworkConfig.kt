package com.example.tugas15_listmoviedb.Konfigurasi
import com.example.tugas15_listmoviedb.Model.DataListMovie
import com.example.tugas15_listmoviedb.Model.ModelDetailMovie
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class NetworkConfig {
    fun getInterceptor() : OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        return okHttpClient
    }

    fun getRetrofit() : Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/movie/")
            .client(getInterceptor())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getListMovie() = getRetrofit().create(MovieList::class.java)
    fun getDetailMovie(movie_id: Int) = getRetrofit().create(MovieList::class.java)
}

interface MovieList {
    @GET("popular?api_key=15444e9f30397ceb4cb830246a6ee4df")
    fun getDataListMovie(): Call<DataListMovie>

    @GET("{id}")
    fun getDataDetailMovie(
        @Path("id") movie_id: Int,
        @Query("api_key") api_key: String = "15444e9f30397ceb4cb830246a6ee4df"
    ): Call<ModelDetailMovie>
}
