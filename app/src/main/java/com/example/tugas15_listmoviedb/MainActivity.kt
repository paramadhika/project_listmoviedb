package com.example.tugas15_listmoviedb

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.tugas15_listmoviedb.Model.DataListMovie
import com.example.tugas15_listmoviedb.Model.ResultsItem
import com.example.tugas15_listmoviedb.MovieInterface.MovieInterface
import com.example.tugas15_listmoviedb.Presenter.MoviePresenter
import kotlinx.android.synthetic.main.activity_main.*

@Suppress("UNCHECKED_CAST")
class MainActivity : AppCompatActivity(), MovieInterface.View {
    private val movieAdapter = MovieAdapter(arrayListOf())
    private lateinit var presenter : MoviePresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        titleMovie.adapter = movieAdapter
        presenter = MoviePresenter(this)

    }

    override fun onStart() {
        super.onStart()
        presenter.getListMovie()
    }

    override fun onResultListMovie(response: DataListMovie) {
        val result = response.results
        movieAdapter.setData(result as List<ResultsItem>)
    }

    override fun onFailureListMovie(t: Throwable) {
        Toast.makeText(
            this,
            "Connection Error",
            Toast.LENGTH_SHORT
        ).show()
    }

}
