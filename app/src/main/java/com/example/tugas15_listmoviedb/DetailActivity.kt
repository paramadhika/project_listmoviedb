package com.example.tugas15_listmoviedb

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.tugas15_listmoviedb.Model.ModelDetailMovie
import com.example.tugas15_listmoviedb.MovieInterface.MovieInterface
import com.example.tugas15_listmoviedb.Presenter.MovieDetailPresenter
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), MovieInterface.DetailView {

    private lateinit var presenter : MovieDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        dtl_judulFilm.text = intent.getStringExtra("Judul")
        Glide.with(posterDetail).load(intent.getStringExtra("Poster")).into(posterDetail)
        Glide.with(backdropPoster).load(intent.getStringExtra("Backdrop")).into(backdropPoster)
        dtl_ratingFilm.text = intent.getStringExtra("Rate")
        dtl_tglrilis.text = intent.getStringExtra("rilis")

        presenter = MovieDetailPresenter(this)
        val id = intent.getStringExtra("id")!!.toInt()

        presenter.getDetailMovie(id)

        back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onResultDetailMovie(response: ModelDetailMovie) {
        dtl_summary.text = response.overview.toString()
        dtl_popularity.text = "Popularitas : " + response.popularity.toString()
        dtl_tagLine.text = "\"${response.tagline}\""

        dtl_genre.text = "Genre : ${response.genres!!.joinToString {
            it!!.name.toString()            
        }}"
    }

    override fun onFailureDetailMovie(throwable: Throwable) {
        Toast.makeText(
            this,
            "Connection Error",
            Toast.LENGTH_SHORT
        ).show()
    }
}
