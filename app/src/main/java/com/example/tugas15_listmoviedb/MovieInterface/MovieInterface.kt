package com.example.tugas15_listmoviedb.MovieInterface

import com.example.tugas15_listmoviedb.Model.DataListMovie
import com.example.tugas15_listmoviedb.Model.ModelDetailMovie

interface MovieInterface {

    interface Presenter{
        fun getListMovie()
    }

    interface PresenterDetailMovie{
        fun getDetailMovie(id : Int)
    }

    interface View{

        fun onResultListMovie(response: DataListMovie)
        fun onFailureListMovie(throwable: Throwable)

    }

    interface DetailView{

        fun onResultDetailMovie(response: ModelDetailMovie)
        fun onFailureDetailMovie(throwable: Throwable)

    }

}