package com.example.tugas15_listmoviedb

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tugas15_listmoviedb.Model.ResultsItem


class MovieAdapter(val data: ArrayList<ResultsItem>) : RecyclerView.Adapter<MovieAdapter.MyHolder>() {
    fun setData(result : List<ResultsItem>){
        data.clear()
        data.addAll(result)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MyHolder(view)
    }
    override fun onBindViewHolder(holder: MovieAdapter.MyHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val judul : TextView = itemView.findViewById(R.id.title)
        private val overview : TextView = itemView.findViewById(R.id.summary)
        private val rating : TextView = itemView.findViewById(R.id.vote)
        private val rilisDate : TextView = itemView.findViewById(R.id.tglrilis)
        private val posterFilm : ImageView = itemView.findViewById(R.id.poster)
        private val movie_id : TextView = itemView.findViewById(R.id.movieid)
        private val klikDetail : CardView = itemView.findViewById(R.id.klikDetail)
        private val img_url = "https://image.tmdb.org/t/p/w500/"

        fun bind(get: ResultsItem){
            judul.text = get.title
            rating.text = "Rating : " + get.voteAverage.toString() + " (${get.voteCount}) "
            rilisDate.text = "Release date : " + get.releaseDate
            overview.text = get.overview
            movie_id.text = get.id.toString()
            Glide.with(itemView).load(img_url + get.posterPath).into(posterFilm)
            klikDetail.setOnClickListener {
                val intent = Intent(itemView.context, DetailActivity::class.java)
                intent.putExtra("Judul", judul.text)
                intent.putExtra("Poster", img_url + get.posterPath)
                intent.putExtra("Backdrop", img_url + get.backdropPath)
                intent.putExtra("Rate", rating.text)
                intent.putExtra("rilis", rilisDate.text)
                intent.putExtra("summary", overview.text)
                intent.putExtra("id", movie_id.text)
                itemView.context.startActivity(intent)
            }
        }
    }
}

