package com.example.tugas15_listmoviedb.Presenter

import android.annotation.SuppressLint
import com.example.tugas15_listmoviedb.Konfigurasi.NetworkConfig
import com.example.tugas15_listmoviedb.Model.ModelDetailMovie
import com.example.tugas15_listmoviedb.MovieInterface.MovieInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailPresenter(var view: MovieInterface.DetailView) : MovieInterface.PresenterDetailMovie {

    override fun getDetailMovie(id: Int) {
        NetworkConfig().getDetailMovie(id)
            .getDataDetailMovie(id)
            .enqueue(object : Callback<ModelDetailMovie> {
                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<ModelDetailMovie>,
                    response: Response<ModelDetailMovie>?
                ) {
                    if (response!!.isSuccessful){
                        view.onResultDetailMovie(response.body()!!)
                    }
                }
                override fun onFailure(call: Call<ModelDetailMovie>, t: Throwable) {
                    view.onFailureDetailMovie(t)
                }
            })
    }

}