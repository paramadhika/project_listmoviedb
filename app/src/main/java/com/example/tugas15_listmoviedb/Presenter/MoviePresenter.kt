package com.example.tugas15_listmoviedb.Presenter

import com.example.tugas15_listmoviedb.Konfigurasi.NetworkConfig
import com.example.tugas15_listmoviedb.Model.DataListMovie
import com.example.tugas15_listmoviedb.MovieInterface.MovieInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviePresenter(var view: MovieInterface.View) : MovieInterface.Presenter {

    override fun getListMovie() {
        NetworkConfig().getListMovie().getDataListMovie()
            .enqueue(object : Callback<DataListMovie> {
                override fun onResponse(
                    call: Call<DataListMovie>?,
                    response: Response<DataListMovie>?
                ) {
                    if(response!!.isSuccessful){
                        view.onResultListMovie(response.body()!!)
                    }
                }
                override fun onFailure(call: Call<DataListMovie>?, t: Throwable) {
                    view.onFailureListMovie(t)
                }
            })
    }
}